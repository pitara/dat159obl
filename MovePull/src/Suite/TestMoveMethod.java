package Suite;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import movePull.Fisk;
import movePull.Lam;
import movePull.Mat;

class TestMoveMethod {

	private static Mat fisk;
	private static Mat lam;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		fisk = new Fisk((int) (Math.random()*100));
		lam  = new Lam((int) (Math.random()*100));
	}

	@Test
	void testFisk() {
		assertEquals(fisk.spise(),195);
	}
	
	@Test
	void testLam() {
		assertEquals(lam.spise(),195);
	}
}
