package Suite;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import movePull.Fisk;
import movePull.Mat;

class TestPullUp {

	private static Mat fisk;
	private static Mat  fiskMat;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		fisk = new Fisk((int) (Math.random()*100));
		fiskMat = new Fisk((int) (Math.random()*100));
	}

	@Test
	public void testFisk() {
		assertEquals(fisk.spise(),195);
	}

}
