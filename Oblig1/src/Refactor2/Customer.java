package Refactor2;

import java.util.Enumeration;
import java.util.Vector;

public class Customer {
    private String _name;
    private Vector _rentals = new Vector();

    public Customer(String name) {
        _name = name;
    }

    public String statement() {
    	
        double totalAmount = 0;
        int frequentRenterPoints = 0;
        Enumeration rentals = _rentals.elements();
        String result = "Rental Record for " + getName() + "\n";
        
 
        
        while (rentals.hasMoreElements()) {
            double thisAmount = 0;
            Rental each = (Rental) rentals.nextElement();
            
     	    int daysRented = each.getDaysRented();
       	    int moviePrice = each.getMovie().getPriceCode();
            thisAmount =  calculate(thisAmount,daysRented,  moviePrice);
            
            // determine amount for each line
            
            

			
			//#3 Extract method  
			thisAmount = calculate(thisAmount, daysRented, moviePrice);

            // add frequent renter points
            frequentRenterPoints ++;
            // add bonus for a two day new release rental
            if ((moviePrice == Movie.NEW_RELEASE) &&
                    daysRented > 1) frequentRenterPoints ++;

            //show figures for this rental
            result += "\t" + each.getMovie().getTitle()+ "\t" +
                    String.valueOf(thisAmount) + "\n";
            totalAmount += thisAmount;
        }
        //add footer lines
        result += "Amount owed is " + String.valueOf(totalAmount) + "\n";
        result += "You earned " + String.valueOf(frequentRenterPoints) +
                " frequent renter points";
        return result;
    }

	private double calculate(double thisAmount, int daysRented, int priceCode) {
		switch (priceCode) {
		    case Movie.REGULAR:
		        thisAmount += 2;
		        if (daysRented > 2)
		            thisAmount += (daysRented - 2) * 1.5;
		        break;
		    case Movie.NEW_RELEASE:
		        thisAmount += daysRented * 3;
		        break;
		    case Movie.CHILDRENS:
		        thisAmount += 1.5;
		        if (daysRented > 3)
		            thisAmount += (daysRented - 3) * 1.5;
		        break;
		}
		return thisAmount;
	}

  
	public void addRental(Rental arg) {
        _rentals.addElement(arg);
    }

    public String getName() {
        return _name;
    }
}
