package Refactor5;


class Regular extends Movie {
	public Regular(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	public double getAmount(int daysRented) {
		  int thisAmount = 2;
          if (daysRented > 2)
              thisAmount += (daysRented - 2) * 1.5;
          return thisAmount;
	}
}
