package Refactor4;
 
import java.util.Enumeration;
import java.util.Vector;

public class Customer {
    private String _name;
    private Vector _rentals = new Vector();

    public Customer(String name) {
        _name = name;
    }

    //#2 Extract method
    public double calculate(Enumeration rentals, double totalAmount,Rental each,int daysRented, int moviePrice ) {
    	 
    	 double thisAmount = 0;
    	
    	       if(moviePrice == 0)  return new Regular(null).getAmount(daysRented);
    	 else if(moviePrice == 1)   return new New_release(null).getAmount(daysRented);
    	 else if(moviePrice == 2)   return new Childrens(null).getAmount(daysRented);
	     throw new IllegalStateException("Illegal moviePrice");
    }
    
    public String statement() {
        double totalAmount = 0;
        int frequentRenterPoints = 0;
        Enumeration rentals = _rentals.elements();
        String result = "Rental Record for " + getName() + "\n";
        while (rentals.hasMoreElements()) {
          
            Rental each = (Rental) rentals.nextElement();
      
            
            //#1 Extract variable
       	    int daysRented = each.getDaysRented();
       	    int moviePrice = each.getMovie().getPriceCode();
            double thisAmount = calculate(rentals, totalAmount, each,daysRented,moviePrice);
            
            // determine amount for each line
           

            // add frequent renter points
            frequentRenterPoints ++;
            // add bonus for a two day new release rental
            if ((moviePrice == 1) &&
            		thisAmount > 1) frequentRenterPoints ++;

            //show figures for this rental
            result += "\t" + each.getMovie().getTitle()+ "\t" +
                    String.valueOf(thisAmount) + "\n";
            totalAmount += thisAmount;
        }
        //add footer lines
        result += "Amount owed is " + String.valueOf(totalAmount) + "\n";
        result += "You earned " + String.valueOf(frequentRenterPoints) +
                " frequent renter points";
        return result;
    }

    public void addRental(Rental arg) {
        _rentals.addElement(arg);
    }

    public String getName() {
        return _name;
    }
}
