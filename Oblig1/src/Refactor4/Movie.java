package Refactor4;

public class Movie {


    private String _title;
    private int _priceCode;

    public Movie(String title) {
        _title = title;
    }

    public int getPriceCode() {
        return _priceCode;
    }

 

    public String getTitle() {
        return _title;
    }
    
    public double getAmount(int daysRented) {
    	throw new IllegalStateException("No subtyping");
    }
}
